package app

import (
	"context"
	"log"
	"net/http"
	"strconv"
	"time"
)

type FormattedTask struct {
	Id        int64
	Task      string
	CreatedAt string
}

func (s *Server) healthHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	_, err := w.Write([]byte("OK"))
	if err != nil {
		log.Printf("Error writing health response: %v", err)
	}
}

func (s *Server) listTasksHandler(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()

	uncompletedTasks, err := s.Db.Queries.ListUncompletedTasks(ctx)
	if err != nil {
		log.Printf("Error listing tasks: %v", err)
		http.Error(w, "Error listing tasks", http.StatusInternalServerError)
	}

	var formattedTasks = make([]FormattedTask, len(uncompletedTasks))
	for i := range uncompletedTasks {
		formattedTasks[i] = FormattedTask{
			Id:        uncompletedTasks[i].ID,
			Task:      uncompletedTasks[i].Task,
			CreatedAt: uncompletedTasks[i].CreatedAt.Format(time.RFC1123),
		}
	}

	w.Header().Set("Content-Type", "text/html")

	err = s.Template.Execute(w, formattedTasks)
	if err != nil {
		log.Printf("Error executing template: %v", err)
		http.Error(w, "Error executing template", http.StatusInternalServerError)
	}
}

func (s *Server) createTaskHandler(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()

	task := r.FormValue("task")

	err := s.Db.Queries.CreateTask(ctx, task)
	if err != nil {
		log.Printf("Error creating task: %v", err)
		http.Error(w, "Error creating task", http.StatusInternalServerError)
	}

	log.Print("Created task")

	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func (s *Server) completeTaskHandler(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()

	id := r.FormValue("id")
	idConverted, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		log.Printf("Error converting task id to int: %v", err)
		http.Error(w, "Error converting task id to int", http.StatusInternalServerError)
	}

	err = s.Db.Queries.CompleteTask(ctx, idConverted)
	if err != nil {
		log.Printf("Error completing task: %v", err)
		http.Error(w, "Error completing task", http.StatusInternalServerError)
	}

	log.Print("Completed task")

	http.Redirect(w, r, "/", http.StatusSeeOther)
}
