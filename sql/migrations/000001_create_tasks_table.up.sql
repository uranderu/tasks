CREATE TABLE IF NOT EXISTS tasks
(
    id           INTEGER PRIMARY KEY,
    task         TEXT     NOT NULL,
    created_at   DATETIME NOT NULL,
    completed_at DATETIME
);

CREATE INDEX idx_tasks_completed_at ON tasks (completed_at);
