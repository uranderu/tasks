-- name: ListUncompletedTasks :many
SELECT id, task, created_at
FROM tasks
WHERE completed_at IS NULL;

-- name: CreateTask :exec
INSERT INTO tasks (task, created_at)
VALUES (?, datetime('now'));

-- name: DeleteTask :exec
DELETE
FROM tasks
WHERE id = ?;

-- name: CompleteTask :exec
UPDATE tasks
set completed_at = datetime('now')
WHERE id = ?;
